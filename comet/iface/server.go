package iface

type IServer interface {
	// Start 启动服务器方法
	Start()
	// Stop 停止服务器方法
	Stop()
	// Serve 开启业务服务方法
	Serve()
	// GetConnMgr 得到链接管理
	GetConnMgr() IConnManager
	// Process 消息处理
	Process(b []byte, c IConnection)

	// OnShutdown OnShutdown在服务器关闭时触发，
	//在所有事件循环和连接关闭后立即调用。
	OnShutdown()

	// OnClosed 当连接关闭时，将触发OnClosed。
	//参数：err是最后一个已知的连接错误。
	//关闭操作在创建连接之后触发，之前无法触发。
	OnClosed(c IConnection) (action Action)

	OnEvent() IEventHandler
	// ConnCodec 协议解码
	ConnCodec(c IConnection)
	PreWrite()

	SendMsgPl(connId int64, b []byte)

	// ServerType 获取服务类型
	ServerType() string
}
