package iface

type Action int

const (
	// None indicates that no action should occur following an event.
	None Action = iota

	// Close closes the connection.
	Close

	// Shutdown shutdowns the server.
	Shutdown
)

// IEventHandler EventHandler表示Serve调用的服务器事件的回调。
//每个事件都有一个Action返回值，该值用于管理连接和服务器的状态。
type IEventHandler interface {
	// OnInitComplete 服务器准备好接受连接时，将触发OnInitComplete。
	//参数：server具有信息和各种实用程序。
	OnInitComplete(server IServer) (action Action)

	// OnShutdown OnShutdown在服务器关闭时触发，
	//在所有事件循环和连接关闭后立即调用。
	OnShutdown(server IServer)

	// OnOpened 当打开新的连接时，将触发OnOpened。
	//参数：c具有有关连接的信息，例如其本地和远程地址。
	//参数：out是将要发送回客户端的返回值。
	//通常不建议您在OnOpened中将大量数据发送回客户端。
	//请注意，由OnOpened返回的字节将被发送回客户端而不进行编码。
	OnOpened(c IConnection)

	// OnClosed 当连接关闭时，将触发OnClosed。
	// 参数：err是最后一个已知的连接错误。
	// 关闭操作在创建连接之后触发，之前无法触发。
	OnClosed(c IConnection) (action Action)

	// PreWrite PreWrite会在将任何数据写入任何客户端套接字之前触发，此事件函数通常用于
	//在将数据写入客户端之前放置一些日志记录，计数/报告或任何前置操作代码。
	PreWrite()

	// Process 消息的处理只是消息，什么也不返回
	Process(frame []byte, c IConnection)
	Codec() ICodec
	//To receive the message, only use the receive message.
	//Receive(frame []byte, c IConnection) (out []byte, action Action)

}
