package iface

import "net"

// IConnection 定义连接接口
type IConnection interface {
	// Start 启动连接，让当前连接开始工作
	Start()
	// Stop 停止连接，结束当前连接状态M
	Stop()

	// GetConnection 从当前连接获取原始的socket TCPConn
	GetConnection() net.Conn
	// GetConnID 获取当前连接ID
	GetConnID() int64
	// RemoteAddr 获取远程客户端地址信息
	RemoteAddr() net.Addr

	// SendBuffMsg 直接将Message数据发送数据给远程的TCP客户端(无缓冲)
	//是否使用Protocol
	//直接将Message数据发送给远程的TCP客户端(有缓冲)
	SendBuffMsg(Protocol bool, data []byte) error

	// InitCodec 初始化协议
	InitCodec(codec ICodec)

	// SetContext Context
	SetContext(interface{})
	Context() interface{}
}
