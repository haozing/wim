package iface

// ICodec 协议解析
type ICodec interface {
	// Encode encodes frames upon server responses into TCP stream.
	// EncodeBuf 协议编译后的字节
	Encode(buf []byte) (EncodeBuf []byte, err error)
	// Decode decodes frames from TCP stream via specific implementation.
	// DecodeBuf 协议解析后的字节
	// BufLen 字节长度
	// DecodeStatus 解析的状态：PackageLess、PackageFull、PackageError
	Decode(buf []byte) (DecodeBuf []byte, BufLen int, DecodeStatus int)
}
