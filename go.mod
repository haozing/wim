module gitee.com/haozing/wim

go 1.16

require (
	github.com/bwmarrin/snowflake v0.3.0
	github.com/gobwas/httphead v0.1.0 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/gobwas/ws v1.0.4
	github.com/golang/glog v0.0.0-20210429001901-424d2337a529
	golang.org/x/sys v0.0.0-20210608053332-aa57babbf139 // indirect
)
