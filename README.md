golang实现的小范围websocket协议，推送服务。

###### 特点：

易部署、依赖少、可扩展

###### 使用：

golang

websocket

tcp
###### 架构：

三个核心可以单独使用

comet:接入层

session:状态层

**消息发送：**

消息->post->sync->session->comet->客户端

**在线维护：**

客户端->comet->session->comet->客户端

**消息推送：**

sync->session->comet->客户端

###### 安装：

###### 协议：


