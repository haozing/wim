package internal

import (
	"fmt"
	"gitee.com/haozing/wim/session/iface"
	"time"
)

type session struct {
	sessionType     int                     //消息的状态，空，push还是
	sessionId       int64                   //sessionId
	sessionState    int                     //消息的状态，
	pendingMessages []iface.IPendingMessage //直接给客户端的消息
	rpcMessages     []interface{}           //给rpcQueue的消息
	syncMessages    []iface.IPendingMessage //推送的消息，给客户端
	connState       int                     //是否在线
	closeDate       int64                   //session关闭时间
	cb              iface.IAuthUser
	lastReceiveTime int64 //最后一条消息的时间
	inSession       iface.InSession
}

func (c *session) LastReceiveTime() int64 {
	return c.lastReceiveTime
}

func newSession(sessionId int64, sessType int, cb iface.IAuthUser, inSession iface.InSession) iface.ISession {
	sb := &session{
		sessionId:       sessionId,
		sessionType:     sessType,
		pendingMessages: []iface.IPendingMessage{},
		cb:              cb,
		sessionState:    iface.KSessionStateNew,
		closeDate:       time.Now().Unix() + iface.KDefaultPingTimeout + iface.KPingAddTimeout,
		// closeSessionDate: 0,
		// lastTime:         time.Now().Unix(),
		connState:       iface.KStateNew,
		lastReceiveTime: time.Now().UnixNano(),
		inSession:       inSession,
	}

	return sb
}
func (c *session) String() string {
	return fmt.Sprintf("{user_id: %d, auth_key_id: %d, session_type: %d, session_id: %d, state: %d, conn_state: %d}",
		c.cb.UserId(),
		c.cb.AuthKeyId(),
		c.sessionType,
		c.sessionId,
		c.sessionState,
		c.connState)
}

func (c *session) Id() int64 {
	return c.sessionId
}

func (c *session) SetId(sessionId int64) {
	c.sessionId = sessionId
}

func (c *session) Type() int {
	return c.sessionType
}

func (c *session) Online() bool {
	return c.connState == iface.KStateOnline
}
func (c *session) SetConnState(i int) {
	c.connState = i
}
func (c *session) ConnState() int {

	return c.connState
}
func (c *session) Closed() bool {
	return c.connState == iface.KStateClose
}

func (c *session) SetInSession(sess iface.InSession) {
	c.inSession = sess
}

func (c *session) InSession() (sess iface.InSession) {
	return c.inSession
}
func (c *session) AuthUser() iface.IAuthUser {
	return c.cb
}
func (c *session) IdExist(connId int64) bool {
	if c.sessionId == connId {
		return true
	} else {
		return false
	}
}
