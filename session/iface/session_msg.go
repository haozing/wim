package iface

const (
	KSessionUnknown      = 0
	KSessionGeneric      = 1
	KSessionDownload     = 2
	KSessionUpload       = 3
	KSessionPush         = 4
	KSessionTemp         = 5
	KSessionProxy        = 6
	KSessionGenericMedia = 7
	// KSessionMaxSize = 8
)

const (
	KDefaultPingTimeout  = 30
	KPingAddTimeout      = 15
	KCacheSessionTimeout = 3 * 60
)

const (
	KStateNew = iota
	KStateOnline
	KStateOffline
	KStateClose
)

const (
	KSessionStateNew = iota
	KSessionStateCreated
)

const (
	KServerSaltIncorrect = int32(48)
)

const (
	KMsgIdTooLow    = int32(16)
	KMsgIdTooHigh   = int32(17)
	KMsgIdMod4      = int32(18)
	KMsgIdCollision = int32(19)

	KMsgIdTooOld = int32(20)

	KSeqNoTooLow  = int32(3)
	KSeqNoTooHigh = int32(33)
	KSeqNoNotEven = int32(34)
	KSeqNoNotOdd  = int32(35)

	KInvalidContainer = int32(64)
)
const (
	KNetworkMessageStateNone             = 0 // created
	KNetworkMessageStateReceived         = 1 // received from client
	KNetworkMessageStateRunning          = 2 // invoke api
	KNetworkMessageStateWaitReplyTimeout = 3 // invoke timeout
	KNetworkMessageStateInvoked          = 4 // invoke ok, send to client
	KNetworkMessageStatePushSync         = 5 // invoke ok, send to client
	KNetworkMessageStateAck              = 6 // received client ack
	KNetworkMessageStateWaitAckTimeout   = 7 // wait ack timeout
	KNetworkMessageStateError            = 8 // invalid error
	KNetworkMessageStateEnd              = 9 // end state
)

type IPendingMessage interface {
	MessageId() int64
	Confirm() bool
	Tl() interface{}
}

type IRpcApiMessages interface {
	SessionId() int64
	SetSessionId(int64)
	RpcMessages() interface{}
	SetRpcMessages(interface{})
}

type IMessage interface {
	MsgId() int64
	SeqNo() int32
	Object() interface{}
}
