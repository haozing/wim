package iface

type IMsgHandle interface {
	AuthKeyId() int64 //AuthKeyId
	SetAuthKeyId(int64)

	Attachment() interface{} //消息
	SetAttachment(interface{})
}
