package iface

type IFutureSalt interface {
	ValidSince() int32
	ValidUntil() int32
	Salt() int64
}
