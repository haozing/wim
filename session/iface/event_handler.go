package iface

type IEventHandler interface {
	InitSession(authUser IAuthUser, msg interface{}) (ins InSession, sess ISession)
	MsgDecodeHandler(authUser IAuthUser, msg interface{}) (sessionId int64, decodeMsg interface{})
	SetOnline(authUser IAuthUser) error
	SetOffline(authUser IAuthUser) error
}
