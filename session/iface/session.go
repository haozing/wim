package iface

type ISession interface {
	Id() int64
	SetId(sessionId int64)
	Type() int
	IdExist(connId int64) bool
	Online() bool
	Closed() bool
	LastReceiveTime() int64
	InSession() InSession
	AuthUser() IAuthUser
	SetConnState(i int)
	ConnState() int
}

type InSession interface {
	SessionId() int64 //
	SessionType() int
	Timer()

	Dispatch(sess ISession, msg IRpcApiMessages) (dMsg IRpcApiMessages, err error)
	OnRpcResult(sess ISession, msg IRpcApiMessages)              //将消息发送到前端网关(comet)
	OnRpcRequest(sess ISession, msg IRpcApiMessages) interface{} //自定义的逻辑处理单元
}
