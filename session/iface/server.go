package iface

type IServer interface {
	CloseAuthUser(int64)
	GetAuthUser(int64) IAuthUser
	Event() IEventHandler
	MsgHandle(m IMsgHandle) error
}
