package iface

type IAuthUser interface {
	GetOnlineSession(int) ISession
	AuthKeyId() int64

	AuthKey() []byte
	SetAuthKey([]byte)

	Context(key string) interface{}
	SetContext(key string, i interface{})

	UserId() int32
	SetUserId(userId int32)

	CacheSalt() IFutureSalt
	SetCacheSalt(f IFutureSalt)

	CacheLastSalt() IFutureSalt
	SetCacheLastSalt(f IFutureSalt)

	Destroy(sessionId int64) bool
	BindPushSessionId(sessionId int64)

	SetOnline()
	SetOffline()

	Start()
	Stop()
	MsgJoin(interface{}) error

	Server() IServer
}
